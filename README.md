# Viz-style-manager

A panel for managing the style of a network from VizCore (in JsonGraph format).

## Props

| Props | Type | default | Description | Optional |
| ----- | ---- | ------- | ----------- | -------- |
| draggable | boolean | false | Allows you to move the panel | Yes |
| nodeClass | {[key: string]: NodeStyle} | {} | Object that contains the association between nodes classes and their respective styles | No |
| linkClass | {[key: string]: LinkStyle} | {} | Object that contains the association between links classes and their respective styles | No |
| x | number | 0 | X position of the panel | Yes |
| y | number | 0 | Y position of the panel | Yes |

## Types 

### LinkStyle

| Attribut | Type | Description |
| -------- | ---- | ----------- |
| display | boolean | Allows you to display or not links |
| stroke | string | Link color |
| strokeWidth | number | Link width |
| opacity | number | Link opacity |

### NodeStyle

| Attribut | Type | Description |
| -------- | ---- | ----------- |
| height | number | Node height |
| width | number | Node width |
| fill | string | Node color |
| strokeWidth | number | Node border width |
| stroke | string | Node border color |
| displayLabel | boolean | Allows you to display or not node's label |
| shape | string | Allows you to choose node's shape |
| opacity | number | Node opacity |

## Events

| Name | Trigger | Output |
| ---- | ------- | ------ |
| @returnNewStyle | change on panel | new style, class associated with, 'node' or 'link' |

## Use the package in another project

```sh
npm i -D @metabohub/viz-style-manager
```

If your project is not using vuetify, you need to import it in `src/main.ts` :
```ts
import { createApp } from 'vue'
import App from './App.vue'
import { vuetify } from "@metabohub/viz-style-manager";

createApp(App).use(vuetify).mount('#app')
```

Use the component : 
```ts
<script setup lang="ts">
import { StyleManager } from "@metabohub/viz-style-manager";
</script>

<template>
  <StyleManager
    :draggable="true"
    :nodeClass="nodeClass"
    :linkClass="linkClass"
    x="150"
    y="50"
    @returnNewStyle="yourFunction"
  />
</template>

<style>
import "@metabohub/component-example/dist/style.css";
</style>
```