import { describe, it, expect } from "vitest";
import StyleManager from "../StyleManager.vue";
import { mount, VueWrapper } from '@vue/test-utils';
import vuetify from "@/plugins/vuetify";

global.ResizeObserver = require('resize-observer-polyfill')

const mockProps = {
  draggable: false,
  nodeClass: {
    metabolite: {
      width: 10,
      height: 10
    }
  },
  linkClass: {
    classicEdge: {
      width: 1,
      stroke: '#0000ff'
    }
  },
  x: 0,
  y: 0
}

describe('StyleManager.vue', () => {
  let component: VueWrapper;
  it('Should import StyleManager component', () => {
    expect(StyleManager).toBeTruthy();
  });

  it('Should mount StyleManager component', () => {
    component = mount(StyleManager, {
      global: {
        plugins: [vuetify]
      },
    });

    expect(component.exists()).toBe(true);
    expect(component.find('.v-card-title').text()).toEqual('Style Manager');
  });

  it('Should mount StyleManager component with props', () => {
    component = mount(StyleManager, {
      props: mockProps,
      global: {
        plugins: [vuetify]
      }
    });

    expect(component.exists()).toBe(true);
  });
});