import { describe, it, expect } from 'vitest';
import { completeNodeClassStyle, completeLinkClassStyle } from '../ManageClassStyle';

describe('ManageClassStyle', () => {

  describe('completeNodeClassStyle', () => {
    it('should return the default node style when no argument is passed', () => {
      const expected = {
        displayLabel: true,
        height: 30,
        width: 30,
        fill: '#FFFFFF',
        strokeWidth: 2,
        stroke: '#000000',
        labelPosition: 'middle',
        opacity: 1,
        label: 'identifier',
        shape: 'circle',
        fontSize: 10,
        fontColor: '#000000',
        fontOpacity: 0.5
      };
      
      const result = completeNodeClassStyle();
      expect(result).toEqual(expected);
    });

    it('should merge the passed style with the default node style', () => {
      const style = {
        height: 50,
        fill: '#FF0000'
      };
      const expected = {
        displayLabel: true,
        height: 50,
        width: 30,
        fill: '#FF0000',
        strokeWidth: 2,
        stroke: '#000000',
        labelPosition: 'middle',
        opacity: 1,
        label: 'identifier',
        shape: 'circle',
        fontSize: 10,
        fontColor: '#000000',
        fontOpacity: 0.5
      };

      const result = completeNodeClassStyle(style);
      expect(result).toEqual(expected);
    });

    it('should override the default node style if a specific property is passed', () => {
      const style = { stroke: '#FF00FF' };
      const expected = {
        displayLabel: true,
        height: 30,
        width: 30,
        fill: '#FFFFFF',
        strokeWidth: 2,
        stroke: '#FF00FF',
        labelPosition: 'middle',
        opacity: 1,
        label: 'identifier',
        shape: 'circle',
        fontSize: 10,
        fontColor: '#000000',
        fontOpacity: 0.5
      };

      const result = completeNodeClassStyle(style);
      expect(result).toEqual(expected);
    });
  });

  describe('completeLinkClassStyle', () => {
    it('should return the default link style when no argument is passed', () => {
      const expected = {
        stroke: '#000000',
        strokeWidth: 2,
        opacity: 0.5
      };

      const result = completeLinkClassStyle();
      expect(result).toEqual(expected);
    });

    it('should merge the passed style with the default link style', () => {
      const style = {
        strokeWidth: 4,
        opacity: 0.5
      };
      const expected = {
        stroke: '#000000',
        strokeWidth: 4,
        opacity: 0.5
      };

      const result = completeLinkClassStyle(style);
      expect(result).toEqual(expected);
    });

    it('should override the default link style if a specific property is passed', () => {
      const style = { stroke: '#00FF00' };
      const expected = {
        stroke: '#00FF00',
        strokeWidth: 2,
        opacity: 0.5
      };

      const result = completeLinkClassStyle(style);
      expect(result).toEqual(expected);
    });
  });
});