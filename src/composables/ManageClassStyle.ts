import type { LinkStyle } from "@/types/LinkStyle";
import type { NodeStyle } from "@/types/NodeStyle";

export function completeNodeClassStyle(style: NodeStyle = {}): NodeStyle {
	const defaultNodeStyle: NodeStyle = {
		height: 30,
		width: 30,
		strokeWidth: 2,
		fill: '#FFFFFF',
		stroke: '#000000',
		displayLabel: true,
		label: 'identifier',
		labelPosition: 'middle',
		opacity: 1,
		shape: 'circle',
		fontSize: 10,
		fontColor: '#000000',
		fontOpacity: 0.5
	};

	return { ...defaultNodeStyle, ...style };
}

export function completeLinkClassStyle(style: LinkStyle = {}): LinkStyle {
  const defaultLinkStyle: LinkStyle = {
		stroke: '#000000',
		strokeWidth: 2,
		opacity: 0.5,
	};

	return { ...defaultLinkStyle, ...style };
}